import { Component } from '@angular/core';

import { AuthService } from '@workspace/core-data';

@Component({
  selector: 'workspace-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Full Stack Portal';
  links = [
    { path: '/login', icon: 'person', label: 'Log In' },
    { path: '/items', icon: 'loyalty', label: 'Items'}
  ];

  constructor(private authService: AuthService) {}

  isAuthenticated$() {
    return this.authService.isAuthenticated$;
  }
}
