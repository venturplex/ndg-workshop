import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { Observable } from 'rxjs';

import { Item } from '@workspace/core-data';
import { ItemsFacade } from '@workspace/core-state';

@Component({
  selector: 'workspace-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss']
})
export class ItemComponent implements OnInit {
  form: FormGroup;
  items$: Observable<Item[]> = this.itemsFacade.items$;
  currentItem$: Observable<Item> = this.itemsFacade.currentItem$;
  isLoading: Observable<boolean> = this.itemsFacade.isItemLoading$;

  constructor(
    private itemsFacade: ItemsFacade,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    this.initForm();
    this.itemsFacade.loadItems();
    this.itemsFacade.mutations$.subscribe(_ => this.resetCurrentItem());
    this.resetCurrentItem();
  }

  resetCurrentItem() {
    this.form.reset();
    this.selectItem({id: null} as Item);
  }

  selectItem(item: Item) {
    this.form.patchValue(item);
    this.itemsFacade.selectItem(item.id);
  }

  saveItem(item: Item) {
    !item.id ?
      this.itemsFacade.createItem(item) :
      this.itemsFacade.updateItem(item);
  }

  deleteItem(item: Item) {
    this.itemsFacade.deleteItem(item);
  }

  private initForm() {
    this.form = this.formBuilder.group({
      id: [null],
      name: ['', Validators.compose([Validators.required])],
      description: ['']
    }, {updateOn: 'blur'});
  }
}
