import { Component, Input, Output, EventEmitter } from '@angular/core';

import { Item } from '@workspace/core-data';

@Component({
  selector: 'workspace-item-list',
  templateUrl: './item-list.component.html',
  styleUrls: ['./item-list.component.scss']
})
export class ItemListComponent {
  @Input() items: Item[];
  @Output() selected = new EventEmitter();
  @Output() deleted = new EventEmitter();

  constructor() {}

  select(item: Item) {
    this.selected.emit(item);
  }

  delete(item: Item) {
    this.deleted.emit(item);
  }
}
