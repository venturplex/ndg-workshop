import { Component, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { Item } from '@workspace/core-data';

@Component({
  selector: 'workspace-item-details',
  templateUrl: './item-details.component.html',
  styleUrls: ['./item-details.component.scss']
})
export class ItemDetailsComponent {
  selectedItem: Item;
  originalName: string;
  @Output() submitted = new EventEmitter();
  @Output() cancelled = new EventEmitter();
  @Input() group: FormGroup;
  @Input() set item(value: Item) {
    if (value) { this.originalName = value.name };
    this.selectedItem = Object.assign({}, value);
  }

  constructor() {}

  submit() {
    if (this.group.valid) {
      this.submitted.emit(this.group.value);
    }
  }

  cancel() {
    this.cancelled.emit();
    this.selectedItem = {id: null} as Item;
  }
}
