import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpResponse, HttpErrorResponse, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';

import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class TokenInterceptorService {
  constructor(public auth: AuthService, private router: Router) { }

  checkAuthorization(event: HttpResponse<any>) {
    if (event.body.errors && event.body.errors[0].message.statusCode === 401) {
      this.auth.logout();
      this.router.navigate(['login']);
    }
  }

  logError(error: HttpErrorResponse) {
    console.log('---- ERROR RESPONSE START ----');
    console.error('STATUS', error.status);
    console.error('MESSAGE', error.message);
    console.log('---- ERROR RESPONSE END ----');
  }

  // intercept request and add token
  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const accessToken = this.auth.getToken();

    request = request.clone({
      setHeaders: {
        Authorization: `Bearer ${accessToken}`,
      }
    });

    return next.handle(request)
      .pipe(
        tap(event => {
          if (event instanceof HttpResponse) {
            this.checkAuthorization(event);
          }
        }, error => {
          this.logError(error);
        })
      );
  }
}
