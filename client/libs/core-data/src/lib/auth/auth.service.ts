import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { map } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs';
import { Apollo } from 'apollo-angular';
import { ApolloQueryResult } from 'apollo-client';

import { loginQuery } from './auth.graphql';

export const TOKEN_NAME = 'token';
export interface User {
  id: string;
  username: string;
  password: string;
  role: string;
  createdAt: string;
  updatedAt: string;
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  isAuthenticated$ = new BehaviorSubject(false);

  constructor(
    private apollo: Apollo,
    private router: Router
  ) {
    this.setToken(this.getToken());
  }

  getToken() {
    return localStorage.getItem(TOKEN_NAME);
  }

  setToken(token: string) {
    localStorage.setItem(TOKEN_NAME, token);
    this.isAuthenticated$.next(token !== '');
  }

  login(user: Partial<User>) {
    return this.apollo
      .query({
        query: loginQuery,
        variables: {
          user,
        }
      }).pipe(
        map((response: ApolloQueryResult<any>) => response.data.login)
      );
  }

  logout() {
    this.setToken('');
    this.router.navigate(['/login']);
  }
}
