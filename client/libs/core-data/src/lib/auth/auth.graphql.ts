import gql from 'graphql-tag';

export const loginQuery = gql`
  query login($user: UserInput!) {
    login(user: $user) {
      token
      user {
        id
        username
        password
        role
        updatedAt
        createdAt
      }
    }
  }
`;
