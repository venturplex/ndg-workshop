import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AuthGuard } from './auth/auth.guard';
import { AuthService } from './auth/auth.service';
import { GraphqlModule } from '@workspace/graphql';
import { TokenInterceptorService } from './auth/token-interceptor.service';
import { NotifyService } from './notify/notify.service';
import { MaterialModule } from '@workspace/material';
import { ItemsService } from './items/items.service';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    GraphqlModule,
    MaterialModule
  ],
  providers: [
    AuthGuard,
    AuthService,
    NotifyService,
    ItemsService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true,
    },
  ],
  exports: [
    HttpClientModule
  ]
})
export class CoreDataModule {}
