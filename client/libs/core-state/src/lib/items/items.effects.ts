import { Injectable } from '@angular/core';

import { map } from 'rxjs/operators';
import { DataPersistence } from '@nrwl/nx';
import { Effect, Actions } from '@ngrx/effects';

import { NotifyService, ItemsService, Item } from '@workspace/core-data';
import { ItemsActionTypes, LoadItems, ItemsLoaded, AddItem, ItemAdded, UpdateItem, ItemUpdated, DeleteItem, ItemDeleted } from './items.actions';
import { ItemsState } from './items.reducer';

@Injectable()
export class ItemsEffects {
  @Effect()
  loadItems$ = this.dataPersistence.fetch(ItemsActionTypes.LOAD_ITEMS, {
    run: (action: LoadItems, state: ItemsState) => {
      return this.itemsService.all().pipe(map((res: Item[]) => new ItemsLoaded(res)));
    },

    onError: (action: LoadItems, error) => {
      this.notifyService.notify(error, 'Error');
    }
  });

  @Effect()
  addItem$ = this.dataPersistence.pessimisticUpdate(ItemsActionTypes.ADD_ITEM, {
    run: (action: AddItem, state: ItemsState) => {
      return this.itemsService.create(action.payload).pipe(map((res: Item) => new ItemAdded(res)));
    },

    onError: (action: AddItem, error) => {
      this.notifyService.notify(error, 'Error');
    }
  });

  @Effect()
  updateItem$ = this.dataPersistence.pessimisticUpdate(ItemsActionTypes.UPDATE_ITEM, {
    run: (action: UpdateItem, state: ItemsState) => {
      return this.itemsService.update(action.payload).pipe(map((res: Item) => new ItemUpdated(res)));
    },

    onError: (action: UpdateItem, error) => {
      this.notifyService.notify(error, 'Error');
    }
  });

  @Effect()
  deleteItem$ = this.dataPersistence.pessimisticUpdate(ItemsActionTypes.DELETE_ITEM, {
    run: (action: DeleteItem, state: ItemsState) => {
      return this.itemsService.delete(action.payload.id).pipe(map(_ => new ItemDeleted(action.payload)));
    },

    onError: (action: DeleteItem, error) => {
      this.notifyService.notify(error, 'Error');
    }
  });

  constructor(
    private actions$: Actions,
    private dataPersistence: DataPersistence<ItemsState>,
    private itemsService: ItemsService,
    private notifyService: NotifyService
  ) { }
}
