import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { Subject } from 'rxjs';
import { tap, takeUntil } from 'rxjs/operators';

import { User, AuthService, NotifyService } from '@workspace/core-data';

@Component({
  selector: 'workspace-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {
  form: FormGroup;
  hide = true;
  destroy$ = new Subject();

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private notifyService: NotifyService,
  ) { }

  ngOnInit() {
    this.initForm();
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.unsubscribe();
  }

  login() {
    if (this.form.valid) {
      this.authService.login(this.form.value)
        .pipe(
          tap((res: {token: string, user: User}) => this.authService.setToken(res.token)),
          tap(_ => this.router.navigate(['/items'])),
          tap((res: {token: string, user: User}) => this.notifyService.notify(`Welcome ${res.user.username}!`)),
          takeUntil(this.destroy$)
        ).subscribe();
    }
  }

  private initForm() {
    this.form = this.formBuilder.group({
      username: ['', Validators.compose([Validators.required])],
      password: ['', Validators.compose([Validators.required])],
    }, {updateOn: 'submit'});
  }

}
