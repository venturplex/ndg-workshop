import * as faker from 'faker';
import { User } from '../src/core/entities/user';
import { Item } from '../src/core/entities/item';

const generateItems = async () => {
  const items = [];

  for (let i = 0, len = faker.random.number({min: 1, max: 25}); i < len; ++i) {
    const itemData = {
      id: faker.random.uuid(),
      name: faker.name.title(),
      description: faker.random.words(),
    };

    items.push(itemData);
  }

  const itemInstances = await Item.bulkCreate(items);
  return itemInstances;
};

const generateUsers = async () => {
  const defaultUsers = [
    {username: 'lruebbelke', password: 'password', role: 'Admin'},
    {username: 'jgarvey', password: 'password', role: 'Supervisor'},
    {username: 'vavila', password: 'password', role: 'User'},
  ];

  const userInstances = await User.bulkCreate(defaultUsers, {ignoreDuplicates: true});
  generateItems();
  return userInstances;
};

export default async () => {
  await generateUsers();
};
