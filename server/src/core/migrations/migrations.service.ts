
import { Injectable, Logger } from '@nestjs/common';

import * as Umzug from 'umzug';
import { Sequelize } from 'sequelize-typescript';

import seedData from '../../../seeds/seeds';

@Injectable()
export class MigrationsService {
  async migrateDatabase(sequelize: Sequelize) {
    Logger.log('Checking database for migrations');

    const ummie = new Umzug({
      logging: (message: any) => Logger.log(message),
      migrations: {
        params: [sequelize.getQueryInterface(), Sequelize],
      },
      storage: 'sequelize',
      storageOptions: {
        sequelize,
        tableName: 'migrations',
      },
    });

    const migrations = await ummie.pending();

    Logger.log(`${migrations.length} migration(s) to run.`);

    if (migrations.length) {
      Logger.log('Running migrations', 'Migration Service');
      await ummie.up();
      Logger.log('Migrations complete', 'Migration Service');
    }

    Logger.log('Database seed starting');

    await seedData();

    Logger.log('Database seed complete');
  }
}
