import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { User } from '../entities/user';
import { JwtPayload } from './jwt-payload.interface';

interface LoginPayload {
  token: string;
  user: User;
}

@Injectable()
export class AuthService {
  loggedInUser: any;

  constructor(private readonly jwtService: JwtService) {}

  setLoggedInUser(user) {
    this.loggedInUser = user;
  }

  getLoggedInUser() {
    return this.loggedInUser;
  }

  async sign(user): Promise<LoginPayload> {
    const token = await this.jwtService.sign({
      id: user.id,
      username: user.username,
      password: user.password,
      role: user.role,
    });

    return await { token, user };
  }

  async login(userData) {
    const user = await User.findOne({where: {username: userData.username}});

    this.setLoggedInUser(user);

    return this.sign(user);
  }

  async validateUser(payload: JwtPayload): Promise<User> {
    const user = await User.findOne({ where: { id: payload.id } });

    this.setLoggedInUser(user);

    return user;
  }
}
