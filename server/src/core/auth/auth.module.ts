import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';

import * as config from 'config';

import { AuthService } from './auth.service';
import { GqlAuthGuard } from './gql-auth.guard';

@Module({
  imports: [
    JwtModule.register({
      secretOrPrivateKey: config.get('jwt.secret'),
      signOptions: {
        expiresIn: 86400, // 24 hours
      },
    }),
  ],
  providers: [AuthService, GqlAuthGuard],
  exports: [AuthService, GqlAuthGuard],
})
export class AuthModule { }
