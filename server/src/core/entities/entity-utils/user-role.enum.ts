export enum UserRole {
  USER = 'User',
  SUPERVISOR = 'Supervisor',
  ADMIN = 'Admin',
}
