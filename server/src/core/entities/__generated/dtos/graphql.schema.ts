
/** ------------------------------------------------------
 * THIS FILE WAS AUTOMATICALLY GENERATED (DO NOT MODIFY)
 * -------------------------------------------------------
 */

/* tslint:disable */
export class ItemInput {
    name: string;
    description?: string;
}

export class ItemPatch {
    id: string;
    name: string;
    description?: string;
}

export class UserInput {
    username: string;
    password: string;
}

export class ItemDTO {
    id: string;
    name: string;
    description?: string;
}

export class LoginPayload {
    token: string;
    user: UserDTO;
}

export abstract class IMutation {
    abstract createItem(input: ItemInput): ItemDTO | Promise<ItemDTO>;

    abstract updateItem(patch: ItemPatch): ItemDTO | Promise<ItemDTO>;

    abstract deleteItem(id: string): string | Promise<string>;
}

export abstract class IQuery {
    abstract login(user: UserInput): LoginPayload | Promise<LoginPayload>;

    abstract allItems(): ItemDTO[] | Promise<ItemDTO[]>;

    abstract temp__(): boolean | Promise<boolean>;
}

export class UserDTO {
    id: string;
    username: string;
    password: string;
    role?: string;
    createdAt?: string;
    updatedAt?: string;
}
