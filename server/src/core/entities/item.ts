import {
  Table,
  Model,
  PrimaryKey,
  AllowNull,
  Default,
  Sequelize,
  Column,
  DataType,
} from 'sequelize-typescript';

@Table({modelName: 'items'})
export class Item extends Model<Item> {
  @PrimaryKey
  @AllowNull(false)
  @Default(Sequelize.fn('uuid_generate_v4'))
  @Column({type: DataType.UUID})
  id: string;

  @Column({type: DataType.STRING})
  name: string;

  @Column({type: DataType.STRING})
  description?: string;
}

export default Item;
