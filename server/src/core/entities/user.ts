import { Table, Model, PrimaryKey, AllowNull, Default, Sequelize, Column, DataType, Unique, CreatedAt, UpdatedAt } from 'sequelize-typescript';
import { UserRole } from './entity-utils/user-role.enum';

@Table({modelName: 'users'})
export class User extends Model<User> {
  @PrimaryKey
  @AllowNull(false)
  @Default(Sequelize.fn('uuid_generate_v4'))
  @Column({type: DataType.UUID})
  id: string;

  @Unique
  @AllowNull(false)
  @Column({type: DataType.STRING})
  username: string;

  @AllowNull(false)
  @Column({type: DataType.STRING})
  password: string;

  @AllowNull(true)
  @Default(UserRole.USER)
  @Column({
    type: DataType.ENUM([UserRole.USER, UserRole.SUPERVISOR, UserRole.ADMIN]),
  })
  role?: string;

  @CreatedAt
  @AllowNull(false)
  @Default(Sequelize.fn('now'))
  @Column({type: DataType.DATE})
  createdAt: Date;

  @UpdatedAt
  @AllowNull(false)
  @Default(Sequelize.fn('now'))
  @Column({type: DataType.DATE})
  updatedAt: Date;
}

export default User;
