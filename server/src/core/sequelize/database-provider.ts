import { Logger } from '@nestjs/common';

import * as config from 'config';
import { Sequelize } from 'sequelize-typescript';

import { DB_CONNECTION_TOKEN } from './constants';

export const databaseProvider = {
  provide: DB_CONNECTION_TOKEN,
  useFactory: async (): Promise<Sequelize> => {
    const url = config.get('database.url');

    const sequelize = new Sequelize({
      dialect: 'postgres',
      logging: (message: string) => Logger.log(message),
      modelPaths: [
        __dirname + `/../entities/`,
      ],
      url,
    });

    return sequelize;
  },
};
