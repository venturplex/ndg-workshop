import { Args, Query, Resolver } from '@nestjs/graphql';

import { User } from '../core/entities/user';
import { AuthService } from '../core/auth/auth.service';

interface LoginPayload {
  token: string;
  user: User;
}

@Resolver('Auth')
export class AuthApiResolvers {
  constructor(private authService: AuthService) {}

  @Query()
  async login(@Args('user') user: any) {
    const payload: LoginPayload = await this.authService.login(user);

    return { token: payload.token, user: payload.user };
  }
}
