import { Module } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';
import { AuthModule } from '../core/auth/auth.module';
import { AuthApiResolvers } from './auth-api.resolver';
import { JwtStrategy } from './jwt.strategy';

@Module({
  imports: [
    PassportModule.register({ defaultStrategy: 'jwt' }),
    AuthModule,
  ],
  providers: [AuthApiResolvers, JwtStrategy],
})
export class AuthApiModule {}
