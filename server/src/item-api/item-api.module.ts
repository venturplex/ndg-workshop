import { Module } from '@nestjs/common';

import { ItemApiResolvers } from './item-api.resolver';
import { AuthModule } from '../core/auth/auth.module';

@Module({
  imports: [
    AuthModule,
  ],
  providers: [
    ItemApiResolvers,
  ],
})
export class ItemApiModule {}
