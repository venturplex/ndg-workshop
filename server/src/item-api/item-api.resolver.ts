import { UseGuards } from '@nestjs/common';
import { Resolver, Query, Mutation, Args } from '@nestjs/graphql';

import { Item } from '../core/entities/item';
import { GqlAuthGuard } from '../core/auth/gql-auth.guard';
import { UserRole } from '../core/entities/entity-utils/user-role.enum';
import { ItemInput, ItemPatch } from '../core/entities/__generated/dtos/graphql.schema';
import { RolesGuard } from '../shared/guards/roles.guard';
import { Roles } from '../shared/decorators/roles.decorator';

@Resolver('item')
export class ItemApiResolvers {
  @Query()
  @UseGuards(new GqlAuthGuard(), RolesGuard)
  @Roles(UserRole.USER, UserRole.SUPERVISOR, UserRole.ADMIN)
  async allItems() {
    return await Item.findAll();
  }

  @Mutation()
  @UseGuards(new GqlAuthGuard(), RolesGuard)
  @Roles(UserRole.SUPERVISOR, UserRole.ADMIN)
  async createItem(@Args('input') input: ItemInput) {
    const item = await Item.create(input);

    return item.dataValues;
  }

  @Mutation()
  @UseGuards(new GqlAuthGuard(), RolesGuard)
  @Roles(UserRole.SUPERVISOR, UserRole.ADMIN)
  async updateItem(@Args('patch') patch: ItemPatch) {
    const item = await Item.findByPk(patch.id);

    if (!item) {
      throw new Error('This item does not exist');
    }

    await item.update(patch);

    return item.dataValues;
  }

  @Mutation()
  @UseGuards(new GqlAuthGuard(), RolesGuard)
  @Roles(UserRole.ADMIN)
  async deleteItem(@Args('id') id: string) {
    const item = await Item.findByPk(id);

    if (!item) {
      throw new Error('This item does not exist');
    }

    await item.destroy();

    return id;
  }
}
