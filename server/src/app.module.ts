import { Module, HttpModule, Inject } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';

import { join } from 'path';
import { Sequelize } from 'sequelize-typescript';

import { AppService } from './app.service';
import { AppController } from './app.controller';
import { DB_CONNECTION_TOKEN } from './core/sequelize/constants';
import { MigrationsService } from './core/migrations/migrations.service';
import { SequelizeCoreModule } from './core/sequelize/sequelize-core.module';
import { AuthApiModule } from './auth-api/auth-api.module';
import { ItemApiModule } from './item-api/item-api.module';

@Module({
  imports: [
    HttpModule,
    SequelizeCoreModule,
    ItemApiModule,
    AuthApiModule,
    GraphQLModule.forRoot({
      playground: true,
      context: ({ req }) => ({ req }),
      typePaths: [`${__dirname}/**/*.graphql`],
      definitions: {
        path: join(process.cwd(), 'src/core/entities/__generated/dtos/graphql.schema.ts'),
        outputAs: 'class',
      },
    }),
  ],
  controllers: [
    AppController,
  ],
  providers: [
    AppService,
    MigrationsService,
  ],
})
export class AppModule {
  constructor(
    private migrationsService: MigrationsService,
    @Inject(DB_CONNECTION_TOKEN) private sequelize: Sequelize,
  ) {
    migrationsService.migrateDatabase(sequelize);
  }
}
