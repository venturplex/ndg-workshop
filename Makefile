help: ## Help documentation
	@echo "Available targets:"
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

audit: ## Run "yarn audit" in all projects
	@(cd client && yarn audit)
	@(cd server && yarn audit)

docker-clean: ## Clean up the last containers for this project
	@docker-compose down --rmi local -v --remove-orphans

install: ## Run "yarn" in all projects
	@(cd client && yarn)
	@(cd server && yarn)

lint: ## Run "yarn lint" in all projects
	@(cd client && yarn run lint)
	@(cd server && yarn run lint)

start: ## Start the containers
	@(COMPOSE_HTTP_TIMEOUT=$$COMPOSE_HTTP_TIMEOUT docker-compose up --remove-orphans --build)

start-clean: docker-clean start ## Clean the docker containers then start
