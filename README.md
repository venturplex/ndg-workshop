# NDG Workshop

Example project to demonstrate a modern fullstack application.

## Prerequisites 

- Install [Docker Desktop](https://www.docker.com/products/docker-desktop)
- Install the Angular CLI `npm install -g @angular/cli`
- Install NX `npm install -g @nrwl/schematics`
- Install the Nest CLI `npm install -g @nestjs/cli`

## Setup

- Install project dependencies: `make install`
- Duplicate and rename `.env.example` to `.env` then type required environment variables (if any).
- Start the project locally by running `make start`
  > (if you would like to clean docker containers before starting run: `make start-clean`)

## Defualt Credentials

```typescript
const defaultUsers = [
  {username: 'lruebbelke', password: 'password', role: 'Admin'},
  {username: 'jgarvey', password: 'password', role: 'Supervisor'},
  {username: 'vavila', password: 'password', role: 'User'},
];
```

## Usage of Makefile

```bash
$ make help

Available targets:
audit                          Run "yarn audit" in all projects
docker-clean                   Clean up the last containers for this project
help                           Help documentation
install                        Run "yarn" in all projects
lint                           Run "yarn lint" in all projects
start                          Start the containers
start-clean                    Clean the docker containers then start
```

## Environment Configuration

Rename and duplicate `server/.env.example` to `server/.env` and insert the appropriate values.
